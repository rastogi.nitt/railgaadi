# railgaadi

# Getting Started

## Requirements

- Python 3.6.6
- PyVenv

### Create a virtual env alongside .git
  use `python3 -m venv RAIL_VENV`, strictly use RAIL_VENV beucase
  folder named `RAIL_VENV` is ignored in the .gitignore

### Install all the requirements
  Use `pip install -r requirements.txt`

### If you add new package using pip 
  use `pip freeze > requirements.txt` to update the requirements.txt


### To run tests use
  `./manage.py test`

### To create migrations
  `./manage.py makemigrations`

### To check migrations already applied to the database 
  `./manage.py showmigraitons`

### To apply migrations to the database
  `./manage.py migrate`

### To create a new admin 
  `./manage.py createsuperuser --username admin`

### To start the django server
  cd `./railgadi` and run `python manage.py runserver`

### To view the proper admin view start the server and go to
  `http://localhost:8000/admin/`

## One time activity

# create a new django project using 
  (Don't run this, its already there, this is just for FYI)
  run `django-admin startporject railgadi`
