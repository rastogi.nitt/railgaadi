import re


# open test file, this a text file which contains information about 'Schdeule' 2

fname = 'schedule_text_only.txt'
with open(fname) as f:
    content = f.readlines()

# Okay, for the content above we got 85 lines, now we filer lines of our interest
filtered_lines = []
for line in content:
    if line.find( 'Item Description:') > -1:
        filtered_lines.append( line )

item_properties = [ "Item Code","Item Qty", "Qty Unit", "Unit Rate", "Basic Value",  "Escl.(%)", "Amount", "Item Description" ]

# for test purpose lets consider only 2 rows
# filtered_lines = filtered_lines[0:2]

# iterate over all the lines
# text before 'Item Description' contains 'Item Code', 'Item Quantity' etc
# The other half contains descripton
item_record_list = []
for line in filtered_lines:
    line = line.strip()
    description_start_index = line.find( 'Item Description:')

    first_half = line[:description_start_index]
    first_half = first_half.strip()

    second_half = line[description_start_index:]
    second_half = second_half.replace( "Item Description:- ", "" )

    # split first_half string based on a 'single space char' the list should have 7 elements
    item_record = first_half.split( " ")
    item_record.append( second_half )

    item_record_list.append( item_record )
    

# for the fun of of lets make write these records in a csv file
f = open( 'out_hsv.txt', 'w')
f.write( "#".join( item_properties ) )
f.write( "\n")

for item_record in item_record_list:
    f.write( "#".join( item_record ) )
    f.write( "\n")

f.close()