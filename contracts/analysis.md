# Contract PDF anaylysis

## Goal 
To automate reading of PDFs which contain contract information and pump them to django app.

## Challenges
The format in which the PDF if made is hard to map directly by a program to a table
Example it has column headers and also an addtional 'Item Desciption' which is a column header but sits in a row

## An approach that would work?

1. Copy the whole PDF to text ( have tested it manually )
2. Focus on the 'Schedule' block and recopy to another text file
3. Look for area between 'Above/ Below/Par' and 'S.No.' 


## Another approach

1. We could look for lines that have 'Item Description:-' string these are the lines we should be interested in.
2. Downside would be we lose all the schedule realted information
3. Implemented in process_contract.py - 
   - Issues 
   1. items which on page breaks
   2. items with unique unit like 'per cm depth/cm width /100 m length' - can be fixed with addtional preprocssing?
