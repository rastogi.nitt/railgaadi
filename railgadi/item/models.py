from django.db import models

MAX_DESCRIPTION_LENGTH = 512

class Item(models.Model):
    name = models.CharField(max_length=20, blank=False)
    description = models.CharField(max_length=MAX_DESCRIPTION_LENGTH, blank=False, null=False, default='standard')

    def __str__(self):
        return self.name

MAX_PROPERTY_VALUE = 20
MAX_NAME_LENGTH = 20

class Property(models.Model):
    item = models.ForeignKey('Item', null=False, blank=False, on_delete=models.CASCADE)
    name = models.CharField(max_length=MAX_NAME_LENGTH, blank=False)
    value = models.CharField(max_length=MAX_PROPERTY_VALUE, blank=False)

    def __str__(self):
        return self.name+self.value

