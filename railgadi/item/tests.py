from django.db import IntegrityError
from django.test import TestCase

from .models import Item

class TestItem(TestCase):
    def test_can_create_item(self):
        item = Item.objects.create(
                name='Test_Item',
                )
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(item.description, 'standard')

        item = Item.objects.create(
                name='Test_Item2',
                )
        self.assertEqual(Item.objects.count(), 2)

    def test_item_is_named_properly(self):
        item = Item.objects.create(
                name='Test_Item',
                )
        self.assertEqual(str(item), item.name)
            

