from django.contrib import admin
from .models import Item, Property

class PropertyInline (admin.TabularInline):
    extra = 1
    model = Property

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    inlines = (PropertyInline,)
    class Meta:
        model = Item

