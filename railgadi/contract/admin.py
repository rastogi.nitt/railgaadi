from django.contrib import admin
from .models import Contract, ContractBill
from bill.models import Bill

class ContractBillInline(admin.TabularInline):
    extra = 1
    model = ContractBill

@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    inlines = (ContractBillInline,)
    class Meta:
        model = Contract

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "tender":
            print('tender_was_here')
            kwargs["queryset"] = Bill.objects.filter(type__iexact='T') # show tenders only
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "bills":
            print('bills_was_here')
            kwargs["queryset"] = Bill.objects.filter(type__iexact='B') # show bills only
        return super().formfield_for_manytomany(db_field, request, **kwargs)
