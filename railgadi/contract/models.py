from django.db import models

MAX_USER_NAME_LENGTH = 20
MAX_CONTRACT_NAME_LENGTH = 30


class Contract(models.Model):
    name = models.CharField(max_length=MAX_USER_NAME_LENGTH, blank=False, null=False)
    created_by = models.CharField(max_length=MAX_USER_NAME_LENGTH, blank=False, null=False)
    tender = models.ForeignKey(
        "bill.Bill", blank=False, null=False, on_delete=models.PROTECT, related_name="tender"
    )
    bills = models.ManyToManyField(to="bill.Bill", through="ContractBill", related_name="bills")

    def __str__(self):
        return name


class ContractBill(models.Model):
    contract = models.ForeignKey("Contract", blank=False, null=False, on_delete=models.PROTECT)
    bill = models.ForeignKey("bill.Bill", blank=False, null=False, on_delete=models.PROTECT)

    def __str__(self):
        return "{}-{}".format(str(contract.id), str(bill.id))
