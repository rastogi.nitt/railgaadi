from django.test import TestCase
from .models import Contract, ContractBill
from bill.models import Bill


class TestContract(TestCase):
    def test_can_create_contract(self):
        contract_name = "tc"
        test_user = "test_user"
        tender = Bill.objects.create(created_by=test_user, approved_by=test_user, approval_status="A")
        bill = Bill.objects.create(created_by=test_user, approved_by=test_user, approval_status="U")
        contract1 = Contract.objects.create(name=contract_name, created_by=test_user, tender=tender)
        ContractBill.objects.create(contract=contract1, bill=bill)

        self.assertEqual(Contract.objects.count(), 1)
        self.assertEqual(ContractBill.objects.count(), 1)
