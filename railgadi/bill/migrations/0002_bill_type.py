# Generated by Django 2.1.2 on 2018-10-12 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bill', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bill',
            name='type',
            field=models.CharField(choices=[('T', 'Tender'), ('B', 'Bill')], default='T', max_length=1),
        ),
    ]
