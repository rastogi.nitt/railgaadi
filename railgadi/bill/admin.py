from django.contrib import admin
from .models import Bill, BillDetails

class BillDetailsInline(admin.TabularInline):
    extra = 1
    model = BillDetails

@admin.register(Bill)
class BillAdmin(admin.ModelAdmin):
    inlines = (BillDetailsInline,)
    class Meta:
        model = Bill

