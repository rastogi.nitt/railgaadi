from django.db import models
from django.core.validators import MinValueValidator

# This should be foreign key to users in user table
# When authentication is implemented properly
MAX_USERNAME_LENGTH = 50

class Bill(models.Model):
    BILL_TYPES = (('T', 'Tender'),
            ('B', 'Bill'))
    DEFAULT_BILL_TYPE = BILL_TYPES[0][0]

    created_by = models.CharField(blank=False, max_length=MAX_USERNAME_LENGTH)
    approved_by = models.CharField(blank=False, max_length=MAX_USERNAME_LENGTH)
    approval_status = models.CharField(max_length=1)
    items  = models.ManyToManyField(to='item.Item', through='BillDetails')
    type = models.CharField(max_length=1, choices=BILL_TYPES, null=False, blank=False, default=DEFAULT_BILL_TYPE)

    @property
    def total_amount(self):
        return sum([item.total_price for item in BillDetails.objects.filter(bill__id=self.id)])

    def __str__(self):
        return 'Bill-'+str(self.id)

class BillDetails(models.Model):
    bill = models.ForeignKey('bill.Bill', null=False, blank=False, default=1, on_delete=models.SET_DEFAULT)
    item = models.ForeignKey('item.Item', null=False, blank=False, default=1, on_delete=models.SET_DEFAULT)
    price_per_unit = models.IntegerField(blank=False,validators=[MinValueValidator(1)])
    quantity = models.IntegerField(blank=False,validators=[MinValueValidator(1)])

    @property
    def total_price(self):
        return self.quantity * self.price_per_unit

