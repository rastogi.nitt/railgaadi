from django.test import TestCase

from ..models import Bill, BillDetails
from item.models import Item

class TestBill(TestCase):
    def test_can_create_item(self):
        bill = Bill.objects.create(
                created_by="test_user",
                approved_by="test_user2",
                approval_status="U")
        self.assertEqual(Bill.objects.count(), 1)

        bill = Bill.objects.create(
                created_by="test_user",
                approved_by="test_user2",
                approval_status="A")
        self.assertEqual(Bill.objects.count(), 2)

    def test_can_add_items_to_bill(self):
        bill = Bill.objects.create(
                created_by="test_user",
                approved_by="test_user2",
                approval_status="U")
        item1 = Item.objects.create(
                name='Test_Item1',
                )
        item2 = Item.objects.create(
                name='Test_Item2',
                )
        item3 = Item.objects.create(
                name='Test_Item3',
                )
        bd1 = BillDetails.objects.create(bill=bill, item=item1, price_per_unit=10, quantity=5)
        bd2 = BillDetails.objects.create(bill=bill, item=item2, price_per_unit=10, quantity=10)
        bd3 = BillDetails.objects.create(bill=bill, item=item3, price_per_unit=10, quantity=2)
        self.assertEqual(Bill.objects.count(), 1)
        self.assertEqual(bd1.total_price, 50)
        self.assertEqual(bd2.total_price, 100)
        self.assertEqual(bd3.total_price, 20)
        self.assertEqual(bill.total_amount, 170)
                

