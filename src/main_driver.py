
# init code
import sqlite3
import json
import config

from item import item

conn = config.conn

def defineSchema():
    c = conn.cursor()
    
    # Create table
    c.execute('''CREATE TABLE items
              ( id integer primary key, properties text )''')

if __name__ == '__main__':
    defineSchema()
