import os

import config
from item import item

conn = config.conn

def test_item_create():
    # create an item
    test_item = item( 1 )

    # save the item
    test_item.save()


def test_item_update():
    # create an item
    test_item = item( 2 )

    # save the item
    test_item.save()

    # update the item
    test_item.property_dict["hello"] = "world"
    
    # save again
    test_item.save()

def test_item_read():
    # 
    prop_dict = {}
    prop_dict["hello"] = "world"
    
    # create an item
    test_item = item( 3 )
    test_item.property_dict = prop_dict

    # save the item
    test_item.save()

    # read the item, not '3' is an existing item
    test_item_2 = item( 3 )
    
    # check if 'test_item_2' is read property
    # This is done by checking if the item read above had the properties of '3' item
    assert test_item_2.property_dict == prop_dict

def test_item_delete():
    # to do
    print "todo"

def test_item_print():
    prop_dict = {}
    prop_dict["hello"] = "world"
    
    # create an item
    test_item = item( 4 )
    test_item.property_dict = prop_dict
    test_item.save()

    print test_item


"""

Steps to run pytest with py-cov plugin

py.test --cov=item --cov-report html .

put people don't have good opinon about using pytest for two things
1. unit tests
2. coverage

OR

We can use native coverage too for python

coverage run --source . -m py.test
coverage report -m
coverage html
"""
