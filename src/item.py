"""

class represnets an item

"""

import sqlite3
import json
import config

conn = config.conn

def find( id ):
    # to check if an item with a given itemid exists
    id_to_search = ( id,)
    c = conn.cursor()
    c.execute( "SELECT EXISTS(SELECT 1 FROM items WHERE id=? LIMIT 1)", id_to_search )
    res = c.fetchone()
    return res[0] != 0

def getItem( id ):
    desired_ids = [ id ]
    c = conn.cursor()
    c.execute('SELECT * FROM items WHERE id=?', desired_ids  )
    res = c.fetchone()
    return res

class item:
    """A simple class representing an item"""

    def __init__( self, id ):
        self.id = id    # instance variable unique to each instance
        self.properties = "{}" # a empty json
        self.property_dict = {}
        self.exists = False # assume item doesn't exist
        
        # this could be good place to contain look up magic
        # if an item with given id exists in DB, load it values and attributes
        if find( id ) == True:
            print "found item in DB"
            # item exists, load it
            res = getItem( id )
            print "loaded"
            self.properties = res[1].encode( 'utf-8' )
            self.property_dict = json.loads( res[1].encode( 'utf-8' ) )
            self.exists = True
        else:
            print "did not find item in DB, don't forget to save"

    def __str__( self ):
        ret_str = ""
        ret_str +=  "ID- " + str( self.id ) + "\n"
        for key,value in self.property_dict.iteritems():
            ret_str += key + "-" + str( value ) + "\n"
        return ret_str

    def save( self ):
        """
        Save the item in DB
        """
        if self.exists == False: # create a new record in the DB
            properties_data = json.dumps( self.property_dict )
            data = ( self.id, properties_data )            
            c = conn.cursor()
            c.execute("INSERT INTO items VALUES (?,?)", data )
            self.exists = True
            print "item saved in DB"

        else: # update the item in DB
            properties_data = json.dumps( self.property_dict )
            id_to_update = [ id ]
            c = conn.cursor()
            c.execute('UPDATE items SET properties=?  WHERE id=?',( properties_data, self.id )  )
            print "item updated in DB"


# to-dos
# 1. implement logger
# 2. How to manage a single conn object
