
"""
Store all the global configs
Follows singleton pattern

"""

import sqlite3

# this could be read from some configuration file test-db/prod-db/stage-db?
db_name = 'example.db'
 
# this DB is created at the current location
conn = sqlite3.connect('example.db')

# define schema
c = conn.cursor()
    
# Create table
c.execute('''CREATE TABLE items
              ( id integer primary key, properties text )''')

